<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220525080550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Created OneToMany';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE list_sizes ADD size_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE list_sizes ADD pictur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE list_sizes ADD CONSTRAINT FK_6F0D136A498DA827 FOREIGN KEY (size_id) REFERENCES sizes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE list_sizes ADD CONSTRAINT FK_6F0D136A9AE76ECE FOREIGN KEY (pictur_id) REFERENCES pictur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6F0D136A498DA827 ON list_sizes (size_id)');
        $this->addSql('CREATE INDEX IDX_6F0D136A9AE76ECE ON list_sizes (pictur_id)');
        $this->addSql('ALTER TABLE "order" ADD buyer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F52993986C755722 FOREIGN KEY (buyer_id) REFERENCES buyer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F52993986C755722 ON "order" (buyer_id)');
        $this->addSql('ALTER TABLE order_composition ADD pictur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_composition ADD order_one_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_composition ADD CONSTRAINT FK_17CD3CC19AE76ECE FOREIGN KEY (pictur_id) REFERENCES pictur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_composition ADD CONSTRAINT FK_17CD3CC13815DEC6 FOREIGN KEY (order_one_id) REFERENCES "order" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_17CD3CC19AE76ECE ON order_composition (pictur_id)');
        $this->addSql('CREATE INDEX IDX_17CD3CC13815DEC6 ON order_composition (order_one_id)');
        $this->addSql('ALTER TABLE review ADD pictur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE review ADD commentator_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C69AE76ECE FOREIGN KEY (pictur_id) REFERENCES pictur (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE review ADD CONSTRAINT FK_794381C6506AFCC0 FOREIGN KEY (commentator_id) REFERENCES commentator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_794381C69AE76ECE ON review (pictur_id)');
        $this->addSql('CREATE INDEX IDX_794381C6506AFCC0 ON review (commentator_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_composition DROP CONSTRAINT FK_17CD3CC19AE76ECE');
        $this->addSql('ALTER TABLE order_composition DROP CONSTRAINT FK_17CD3CC13815DEC6');
        $this->addSql('DROP INDEX IDX_17CD3CC19AE76ECE');
        $this->addSql('DROP INDEX IDX_17CD3CC13815DEC6');
        $this->addSql('ALTER TABLE order_composition DROP pictur_id');
        $this->addSql('ALTER TABLE order_composition DROP order_one_id');
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F52993986C755722');
        $this->addSql('DROP INDEX IDX_F52993986C755722');
        $this->addSql('ALTER TABLE "order" DROP buyer_id');
        $this->addSql('ALTER TABLE list_sizes DROP CONSTRAINT FK_6F0D136A498DA827');
        $this->addSql('ALTER TABLE list_sizes DROP CONSTRAINT FK_6F0D136A9AE76ECE');
        $this->addSql('DROP INDEX IDX_6F0D136A498DA827');
        $this->addSql('DROP INDEX IDX_6F0D136A9AE76ECE');
        $this->addSql('ALTER TABLE list_sizes DROP size_id');
        $this->addSql('ALTER TABLE list_sizes DROP pictur_id');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C69AE76ECE');
        $this->addSql('ALTER TABLE review DROP CONSTRAINT FK_794381C6506AFCC0');
        $this->addSql('DROP INDEX IDX_794381C69AE76ECE');
        $this->addSql('DROP INDEX IDX_794381C6506AFCC0');
        $this->addSql('ALTER TABLE review DROP pictur_id');
        $this->addSql('ALTER TABLE review DROP commentator_id');
    }
}
