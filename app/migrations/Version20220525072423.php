<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220525072423 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Changed table order';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" ALTER delivery TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE "order" ALTER delivery DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN "order".delivery IS NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "order" ALTER delivery TYPE TEXT');
        $this->addSql('ALTER TABLE "order" ALTER delivery DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN "order".delivery IS \'(DC2Type:array)\'');
    }
}
