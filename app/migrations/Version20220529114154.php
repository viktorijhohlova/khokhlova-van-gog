<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220529114154 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE delivery_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE stretcher_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE delivery (id INT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE stretcher (id INT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE "order" ADD delivery_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE "order" ALTER delivery_old SET NOT NULL');
        $this->addSql('ALTER TABLE "order" ALTER sum_order SET NOT NULL');
        $this->addSql('ALTER TABLE "order" ALTER date_order SET NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398ECFE8C54 FOREIGN KEY (delivery_order_id) REFERENCES delivery (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F5299398ECFE8C54 ON "order" (delivery_order_id)');
        $this->addSql('ALTER TABLE order_composition ADD stretcher_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_composition ADD CONSTRAINT FK_17CD3CC1FDB5ED79 FOREIGN KEY (stretcher_id) REFERENCES stretcher (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_17CD3CC1FDB5ED79 ON order_composition (stretcher_id)');
        $this->addSql('ALTER TABLE review ALTER date_review SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398ECFE8C54');
        $this->addSql('ALTER TABLE order_composition DROP CONSTRAINT FK_17CD3CC1FDB5ED79');
        $this->addSql('DROP SEQUENCE delivery_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE stretcher_id_seq CASCADE');
        $this->addSql('DROP TABLE delivery');
        $this->addSql('DROP TABLE stretcher');
        $this->addSql('DROP INDEX IDX_F5299398ECFE8C54');
        $this->addSql('ALTER TABLE "order" DROP delivery_order_id');
        $this->addSql('ALTER TABLE "order" ALTER delivery_old DROP NOT NULL');
        $this->addSql('ALTER TABLE "order" ALTER sum_order DROP NOT NULL');
        $this->addSql('ALTER TABLE "order" ALTER date_order DROP NOT NULL');
        $this->addSql('DROP INDEX IDX_17CD3CC1FDB5ED79');
        $this->addSql('ALTER TABLE order_composition DROP stretcher_id');
        $this->addSql('ALTER TABLE review ALTER date_review DROP NOT NULL');
    }
}
