<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220525061625 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Created picture table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE pictur_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE pictur (id INT NOT NULL, title VARCHAR(255) NOT NULL, year INT NOT NULL, size_original VARCHAR(10) NOT NULL, description TEXT NOT NULL, picture_img VARCHAR(255) NOT NULL, stretcher BOOLEAN NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE pictur_id_seq CASCADE');
        $this->addSql('DROP TABLE pictur');
    }
}
