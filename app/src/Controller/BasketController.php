<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderComposition;
use App\Form\BuyerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    /**
     * @Route("/basket", name="basket_action", methods={"GET", "POST"})
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $order = max($em->getRepository(Order::class)->findAll());
        $order_composition = $em->getRepository(OrderComposition::class)->findBy(array('order_one' => $order));
        $sum_order_composition = 0;
        foreach($order_composition as $elem)
            $sum_order_composition += $elem->getPrice();

        //Создание покупателя
        $form = $this->createForm(BuyerType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $buyer = $form->getData();
            $order->setBuyer($buyer)
            ->setSumOrder($sum_order_composition);
            $em->persist($buyer);
            $em->flush();
        }

        return $this->renderForm('secondary_pages/basket.html.twig', [
            'order_composition' => $order_composition,
            'sum_order_composition' => $sum_order_composition,
            'form_create' => $form]);
    }
}