<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\ListSizes;
use App\Entity\Order;
use App\Entity\Pictur;
use App\Entity\Review;
use App\Entity\Stretcher;
use App\Form\CommentatorType;
use App\Form\OrderCompositionType;
use App\Form\ReviewType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    /**
     * @Route("/gallery", name="gallery_action", methods={"GET"})
     */
    public function executeGallery(EntityManagerInterface $em): Response
    {
        $picture = $em->getRepository(Pictur::class)->findAll();

        return $this->render('secondary_pages/gallery.html.twig', ['picture' => $picture, 'sort_name' => false, 'sort_year' => false]);
    }

    /**
     * @Route("/gallery/sort_by_name", name="sort_name", methods={"GET"})
     */
    public function executeSortName(EntityManagerInterface $em): Response
    {
        $picture = $em->getRepository(Pictur::class)->findAll();

        return $this->render('secondary_pages/gallery.html.twig', ['picture' => $picture, 'sort_name' => true, 'sort_year' => false]);
    }

    /**
     * @Route("/gallery/sort_by_year", name="sort_year", methods={"GET"})
     */
    public function executeSortYear(EntityManagerInterface $em): Response
    {
        $picture = $em->getRepository(Pictur::class)->findAll();

        return $this->render('secondary_pages/gallery.html.twig', ['picture' => $picture, 'sort_name' => false, 'sort_year' => true]);
    }

    /**
     * @Route("/gallery/{id}", name="picture_action", methods={"GET", "POST"})
     */
    public function executePicture(Request $request, EntityManagerInterface $em, int $id): Response
    {
        //Заполнение шаблона с информацией о картине
        $picture = $em->getRepository(Pictur::class)->find($id);
        $list_sizes = $em->getRepository(ListSizes::class)->findBy(array('pictur' => $id));
        $list_stretcher = $em->getRepository(Stretcher::class)->findAll();

        //Создание заказа
        $order_list = $em->getRepository(Order::class)->findAll();
        if (max($order_list)->getSumOrder() == 0){
            $order = max($order_list);
        }else{
            $order = new Order();
            $order->setSumOrder(0);
            $em->persist($order);
            $em->flush();
        }

        //Создание состава заказа
        $form_order_composition = $this->createForm(OrderCompositionType::class);
        $form_order_composition->handleRequest($request);

        if ($form_order_composition->isSubmitted()){
            $order_composition = $form_order_composition->getData();
            if (count($em->getRepository(Stretcher::class)->findBy(array('price' => $order_composition->getPrice()))) > 0){
                $order_composition -> setStretcher($em->getRepository(Stretcher::class)->find(2));
            } else {
                $order_composition -> setStretcher($em->getRepository(Stretcher::class)->find(1));
            }
            $order_composition->setQuantity(1)
                ->setPictur($picture)
                ->setOrderOne($order);
            $em->persist($order_composition);
            $em->flush();
        }

        //Создание отзыва и регистрация комментатора
        $form = $this->createForm(CommentatorType::class);
        $form->handleRequest($request);

        $form_review = $this->createForm(ReviewType::class);
        $form_review->handleRequest($request);

        if ($form->isSubmitted() && $form_review->isSubmitted()) {
            $commentator = $form->getData();
            $em->persist($commentator);

            $review = $form_review->getData();
            if ($commentator){
                $review->setPictur($picture)
                        ->setCommentator($commentator);
                $em->persist($review);
                $em->flush();
            }
        }

        $review = $em->getRepository(Review::class)->findBy(array('pictur' => $id));

        return $this->render('pictures/picture.html.twig', [
            'picture' => $picture,
            'sizes'=>$list_sizes,
            'stretchers'=>$list_stretcher,
            'form_order_composition'=>$form_order_composition->createView(),
            'review'=>$review,
            'form_commentator' =>$form->createView(),
            'form_review' =>$form_review->createView()
            ] );
    }
}