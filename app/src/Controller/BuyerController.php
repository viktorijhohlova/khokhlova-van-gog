<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Buyer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BuyerController extends AbstractController
{
    /**
     * @Route("/buyer/{id}/read", name="buyer_read", methods={"GET"})
     */
    public function readItem(EntityManagerInterface $em, int $id): Response
    {
        $buyer = $em->getRepository(Buyer::class)->find($id);

        return new Response($buyer ? $buyer->getName() : 'Покупатель не найден');
    }

    /**
     * @Route("/buyer/{id}/update", name="buyer_update", methods={"GET"})
     */
    public function updateItem(EntityManagerInterface $em, int $id): Response
    {
        $buyer = $em->getRepository(Buyer::class)->find($id);

        if ($buyer){
            $buyer->setEmail('updating@email.com');
            $em->persist($buyer);
            $em->flush();
            return new Response('Покупатель изменен');
        }

        return new Response($buyer ? $buyer->getEmail():'Покупатель не найден');
    }

    /**
     * @Route("/buyer/{id}/delete", name="buyer_delete", methods={"GET"})
     */
    public function deleteItem(EntityManagerInterface $em, int $id): Response
    {
        $buyer = $em->getRepository(Buyer::class)->find($id);

        if ($buyer){
            $em->remove($buyer);
            $em->flush();
            return new Response('Покупатель удален');
        }

        return new Response($buyer ? $buyer->getName():'Покупатель не найден');
    }
}