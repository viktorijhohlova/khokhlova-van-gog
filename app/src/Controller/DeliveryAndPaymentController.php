<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DeliveryAndPaymentController extends AbstractController
{
    /**
     * @Route("/delivery_and_payment", name="delivery_and_payment_action", methods={"GET"})
     */
    public function executeDeliveryAndPayment(): Response
    {
        return $this->render('secondary_pages/delivery_and_payment.html.twig');
    }

}