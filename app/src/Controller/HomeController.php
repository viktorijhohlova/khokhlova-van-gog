<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home_action", methods={"GET", "POST"})
     */
    public function executeHome(): Response
    {
        return $this->render('secondary_pages/home.html.twig');
    }

}