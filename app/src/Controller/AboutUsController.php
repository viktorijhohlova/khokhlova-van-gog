<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractController
{
    /**
     * @Route("/about_us", name="about_us_action", methods={"GET"})
     */
    public function executeAboutUs(): Response
    {
        return $this->render('secondary_pages/about_us.html.twig');
    }

}