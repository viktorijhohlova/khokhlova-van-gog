<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutHimController extends AbstractController
{
    /**
     * @Route("/about_him", name="about_him_action", methods={"GET"})
     */
    public function executeAboutHim(): Response
    {
        return $this->render('secondary_pages/about_him.html.twig');
    }

}