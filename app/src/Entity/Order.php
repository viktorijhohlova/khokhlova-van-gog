<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    private ?int $sum_order = null;

    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $date_order = null;

    #[ORM\ManyToOne(targetEntity: Buyer::class, inversedBy: 'orders')]
    private ?Buyer $buyer;

    #[ORM\OneToMany(mappedBy: 'order_one', targetEntity: OrderComposition::class)]
    private Collection $order_compositions;

    #[ORM\ManyToOne(targetEntity: Delivery::class, inversedBy: 'orders')]
    private ?Delivery $delivery_order;

    public function __construct()
    {
        $this->order_compositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSumOrder(): ?int
    {
        return $this->sum_order;
    }

    public function setSumOrder(int $sum_order): self
    {
        $this->sum_order = $sum_order;

        return $this;
    }

    public function getDateOrder(): ?\DateTimeInterface
    {
        return $this->date_order;
    }

    public function setDateOrder(\DateTimeInterface $date_order): self
    {
        $this->date_order = $date_order;

        return $this;
    }

    public function getBuyer(): ?Buyer
    {
        return $this->buyer;
    }

    public function setBuyer(?Buyer $buyer): self
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * @return Collection<int, OrderComposition>
     */
    public function getOrderCompositions(): Collection
    {
        return $this->order_compositions;
    }

    public function addOrderComposition(OrderComposition $orderComposition): self
    {
        if (!$this->order_compositions->contains($orderComposition)) {
            $this->order_compositions[] = $orderComposition;
            $orderComposition->setOrderOne($this);
        }
        return $this;
    }

    public function removeOrderComposition(OrderComposition $orderComposition): self
    {
        if ($this->order_compositions->removeElement($orderComposition)) {
            // set the owning side to null (unless already changed)
            if ($orderComposition->getOrderOne() === $this) {
                $orderComposition->setOrderOne(null);
            }
        }
        return $this;
    }

    public function getDeliveryOrder(): ?Delivery
    {
        return $this->delivery_order;
    }

    public function setDeliveryOrder(?Delivery $delivery_order): self
    {
        $this->delivery_order = $delivery_order;

        return $this;
    }
}
