<?php

namespace App\Entity;

use App\Repository\ReviewRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReviewRepository::class)]
class Review
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $date_review;

    #[ORM\Column(type: 'text')]
    private ?string $text_review = null;

    #[ORM\ManyToOne(targetEntity: Pictur::class, inversedBy: 'reviews')]
    private ?Pictur $pictur;

    #[ORM\ManyToOne(targetEntity: Commentator::class, inversedBy: 'reviews')]
    private ?Commentator $commentator;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReview(): ?\DateTimeInterface
    {
        return $this->date_review;
    }

    public function setDateReview(\DateTimeInterface $date_review): self
    {
        $this->date_review = $date_review;

        return $this;
    }

    public function getTextReview(): ?string
    {
        return $this->text_review;
    }

    public function setTextReview(string $text_review): self
    {
        $this->text_review = $text_review;

        return $this;
    }

    public function getPictur(): ?Pictur
    {
        return $this->pictur;
    }

    public function setPictur(?Pictur $pictur): self
    {
        $this->pictur = $pictur;

        return $this;
    }

    public function getCommentator(): ?Commentator
    {
        return $this->commentator;
    }

    public function setCommentator(?Commentator $commentator): self
    {
        $this->commentator = $commentator;

        return $this;
    }
}
