<?php

namespace App\Entity;

use App\Repository\SizesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SizesRepository::class)]
class Sizes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    private ?int $height = null;

    #[ORM\Column(type: 'integer')]
    private ?int $width = null;

    #[ORM\OneToMany(mappedBy: 'size', targetEntity: ListSizes::class)]
    private Collection $list_sizes;

    public function __construct()
    {
        $this->list_sizes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHeight(): ?int
    {
        return $this->height;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return Collection<int, ListSizes>
     */
    public function getListSizes(): Collection
    {
        return $this->list_sizes;
    }

    public function addListSize(ListSizes $listSize): self
    {
        if (!$this->list_sizes->contains($listSize)) {
            $this->list_sizes[] = $listSize;
            $listSize->setSize($this);
        }

        return $this;
    }

    public function removeListSize(ListSizes $listSize): self
    {
        if ($this->list_sizes->removeElement($listSize)) {
            // set the owning side to null (unless already changed)
            if ($listSize->getSize() === $this) {
                $listSize->setSize(null);
            }
        }

        return $this;
    }
}
