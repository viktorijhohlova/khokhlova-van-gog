<?php

namespace App\Entity;

use App\Repository\OrderCompositionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderCompositionRepository::class)]
class OrderComposition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    private ?int $quantity = null;

    #[ORM\Column(type: 'integer')]
    private ?int $price = null;

    #[ORM\ManyToOne(targetEntity: Pictur::class, inversedBy: 'order_composition')]
    private ?Pictur $pictur;

    #[ORM\ManyToOne(targetEntity: Order::class, inversedBy: 'order_compositions')]
    private ?Order $order_one;

    #[ORM\ManyToOne(targetEntity: Stretcher::class, inversedBy: 'order_compositions')]
    private ?Stretcher $stretcher;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPictur(): ?Pictur
    {
        return $this->pictur;
    }

    public function setPictur(?Pictur $pictur): self
    {
        $this->pictur = $pictur;

        return $this;
    }

    public function getOrderOne(): ?Order
    {
        return $this->order_one;
    }

    public function setOrderOne(?Order $order_one): self
    {
        $this->order_one = $order_one;

        return $this;
    }

    public function getStretcher(): ?Stretcher
    {
        return $this->stretcher;
    }

    public function setStretcher(?Stretcher $stretcher): self
    {
        $this->stretcher = $stretcher;

        return $this;
    }
}
