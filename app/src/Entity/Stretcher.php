<?php

namespace App\Entity;

use App\Repository\StretcherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StretcherRepository::class)]
class Stretcher
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    #[ORM\Column(type: 'integer')]
    private ?int $price;

    #[ORM\OneToMany(mappedBy: 'stretcher', targetEntity: OrderComposition::class)]
    private Collection $order_compositions;

    public function __construct()
    {
        $this->order_compositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, OrderComposition>
     */
    public function getOrderCompositions(): Collection
    {
        return $this->order_compositions;
    }

    public function addOrderComposition(OrderComposition $orderComposition): self
    {
        if (!$this->order_compositions->contains($orderComposition)) {
            $this->order_compositions[] = $orderComposition;
            $orderComposition->setStretcher($this);
        }

        return $this;
    }

    public function removeOrderComposition(OrderComposition $orderComposition): self
    {
        if ($this->order_compositions->removeElement($orderComposition)) {
            // set the owning side to null (unless already changed)
            if ($orderComposition->getStretcher() === $this) {
                $orderComposition->setStretcher(null);
            }
        }

        return $this;
    }
}
