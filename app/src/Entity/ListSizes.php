<?php

namespace App\Entity;

use App\Repository\ListSizesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ListSizesRepository::class)]
class ListSizes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'integer')]
    private ?int $price = null;

    #[ORM\ManyToOne(targetEntity: Sizes::class, inversedBy: 'list_sizes')]
    private ?Sizes $size;

    #[ORM\ManyToOne(targetEntity: Pictur::class, inversedBy: 'list_sizes')]
    private ?Pictur $pictur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?Sizes
    {
        return $this->size;
    }

    public function setSize(?Sizes $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getPictur(): ?Pictur
    {
        return $this->pictur;
    }

    public function setPictur(?Pictur $pictur): self
    {
        $this->pictur = $pictur;

        return $this;
    }
}
