<?php

namespace App\Entity;

use App\Repository\PicturRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PicturRepository::class)]
class Pictur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: 'integer')]
    private ?int $year = null;

    #[ORM\Column(type: 'string', length: 10)]
    private ?string $size_original = null;

    #[ORM\Column(type: 'text')]
    private ?string $description = null;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $picture_img = null;

    #[ORM\OneToMany(mappedBy: 'pictur', targetEntity: ListSizes::class)]
    private Collection $list_sizes;

    #[ORM\OneToMany(mappedBy: 'pictur', targetEntity: Review::class)]
    private Collection $reviews;

    #[ORM\OneToMany(mappedBy: 'pictur', targetEntity: OrderComposition::class)]
    private Collection $order_composition;

    public function __construct()
    {
        $this->list_sizes = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->order_composition = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getSizeOriginal(): ?string
    {
        return $this->size_original;
    }

    public function setSizeOriginal(string $size_original): self
    {
        $this->size_original = $size_original;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPictureImg(): ?string
    {
        return $this->picture_img;
    }

    public function setPictureImg(string $picture_img): self
    {
        $this->picture_img = $picture_img;

        return $this;
    }

    /**
     * @return Collection<int, ListSizes>
     */
    public function getListSizes(): Collection
    {
        return $this->list_sizes;
    }

    public function addListSize(ListSizes $listSize): self
    {
        if (!$this->list_sizes->contains($listSize)) {
            $this->list_sizes[] = $listSize;
            $listSize->setPictur($this);
        }

        return $this;
    }

    public function removeListSize(ListSizes $listSize): self
    {
        if ($this->list_sizes->removeElement($listSize)) {
            // set the owning side to null (unless already changed)
            if ($listSize->getPictur() === $this) {
                $listSize->setPictur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): self
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews[] = $review;
            $review->setPictur($this);
        }

        return $this;
    }

    public function removeReview(Review $review): self
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getPictur() === $this) {
                $review->setPictur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OrderComposition>
     */
    public function getOrderComposition(): Collection
    {
        return $this->order_composition;
    }

    public function addOrderComposition(OrderComposition $orderComposition): self
    {
        if (!$this->order_composition->contains($orderComposition)) {
            $this->order_composition[] = $orderComposition;
            $orderComposition->setPictur($this);
        }

        return $this;
    }

    public function removeOrderComposition(OrderComposition $orderComposition): self
    {
        if ($this->order_composition->removeElement($orderComposition)) {
            // set the owning side to null (unless already changed)
            if ($orderComposition->getPictur() === $this) {
                $orderComposition->setPictur(null);
            }
        }

        return $this;
    }
}
