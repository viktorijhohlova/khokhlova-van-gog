<?php
declare(strict_types=1);

namespace App\Form;

use App\Entity\OrderComposition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderCompositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', IntegerType::class, ['attr' => ['class'=>'form-control-lg border-0'],
                                                       'label' => 'Цена',
                                                        'label_attr' => ['class' => 'col-sm-2 col-form-label']])
            ->add('save', SubmitType::class, ['label' => 'Заказать',
                                                        'attr' => ['class' => 'btn btn-lg ml-2 btn-outline-dark mt-4 pictur-font']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver):void
    {
        $resolver->setDefaults([
            'data_class' => OrderComposition::class
        ]);
    }
}
